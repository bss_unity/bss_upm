using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

namespace BSS.Upm {
    public static class UpmUtility 
    {
        [MenuItem("Assets/Create/UPM/Create Project",priority =1)]
        public static void CreateUpmProject() {
            string path= GetCurrentAssetPath();
            Directory.CreateDirectory(path + "/New Project");
            string projectPath= path + "/New Project";
            Directory.CreateDirectory(projectPath + "/Editor");
            Directory.CreateDirectory(projectPath + "/Runtime");
            var pakageFile=File.CreateText(projectPath + "/package.json");
            pakageFile.Write("{}");
            pakageFile.Close();
            File.CreateText(projectPath + "/README.md");
            File.CreateText(projectPath + "/CHANGELOG.md");
            File.CreateText(projectPath + "/LICENSE.md");
            AssetDatabase.Refresh();
        }

        private static string GetCurrentAssetPath() {
            string path = Application.dataPath;
            if(Selection.activeObject != null) {
                path = AssetDatabase.GetAssetPath(Selection.activeObject);
            }
            return path;
        }
    }
}
